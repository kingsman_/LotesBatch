 @echo off
title Malware Removal Tool by :: SmartGenius ::
color 0a

if exist %windir%\winlogon.exe (goto sip) else (goto nop)

:sip
cls
echo.
echo Se ha detectado el Virus WINLOGON
echo Se procedera a erradicarlo del sistema, pero este
echo metodo debe hacerse en modo aprueba de errores ya
echo que este proceso emula a otro que es del sistema
echo y no se dejara borrar tan facil.
echo.
pause
echo.
echo Deshabilitando el sistema mientras se realiza la desinfeccion...
ping -n 1 0.0.0.0 > nul
taskkill /f /im explorer.exe
del /f /q %windir%\winlogon.exe
reg delete HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Run /v runwinlogon /f
ping -n 2 0.0.0.0 > nul
explorer
goto nop

:nop
cls
echo.
echo El virus WINLOGON no se encuentra o
echo ya ha sido eliminado exitosamente
echo.
pause
exit
  