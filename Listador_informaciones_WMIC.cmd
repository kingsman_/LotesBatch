:: (c) Norfipc 2010 - http://norfipc.com
:: Use at your own risk. No warranty express or implied.
:: Permission granted to copy, distribute and modify, provided 
:: this message is not removed.

@ECHO OFF
mode con cols=55 lines=25
:start
cls
echo Que informacion necesitas listar?
echo.
echo 1. Informacion General
echo 2. Sobre el BIOS.
echo 3. Sobre la CPU.
echo 4. Las unidades de CDROM.
echo 5. Redes.
echo 6. Informacion del sistema.
echo 7. Unidades de disco.
echo 8. Discos Logicos
echo 9. Memoria fisica instalada.
echo 10. Servicios.
echo 11. Procesos.
echo 12. Dispositivos en el Board.
echo 13. Programas en el inicio.
echo 20. Errores en el sistema operativo.
echo.
echo 0. Salir.
echo.
set/p valor=           Escriba el numero determinado y pulse ENTER.
if %VALOR%== 0 goto SALIR
if %VALOR%== 1 goto ALL
if %VALOR%== 2 goto BIOS
if %VALOR%== 3 goto CPU
if %VALOR%== 4 goto CDROM
if %VALOR%== 5 goto REDES
if %VALOR%== 6 goto SYSTEM
if %VALOR%== 7 goto DISKDRIVE
if %VALOR%== 8 goto LOGICALDISK
if %VALOR%== 9 goto MEMPHYSICAL
if %VALOR%== 10 goto SERVICE
if %VALOR%== 11 goto PROCESS
if %VALOR%== 12 goto ONBOARDDEVICE
if %VALOR%== 13 goto STARTUP
if %VALOR%== 20 goto RECOVEROS
if %VALOR% GTR 20 echo Error esa opcion no existe
pause
goto start

pause>nul

:ALL
WMIC /Output:ALL.txt  OS get /all /format:LIST
goto start

:BIOS
WMIC /Output:BIOS.txt  BIOS get /all /format:LIST
goto start

:CPU
WMIC /Output:CPU.txt  CPU get /all /format:LIST
goto start

:CDROM
WMIC /Output:CDROM.txt  CDROM get /all /format:LIST
goto start

:REDES
WMIC /Output:REDES.txt  NICCONFIG get /all /format:LIST
goto start

:SYSTEM
WMIC /Output:SYSTEM.txt  COMPUTERSYSTEM get /all /format:LIST
goto start

:DISKDRIVE
WMIC /Output:DISKDRIVE.txt  DISKDRIVE get /all /format:LIST
goto start

:MEMPHYSICAL
WMIC /Output:MEMORIA.txt  MEMPHYSICAL get /all /format:LIST
goto start

:SERVICE
WMIC /Output:SERVICIOS.txt  SERVICE get /all /format:LIST
goto start

:ONBOARDDEVICE
WMIC /Output:DispositivosEnBoard.txt  ONBOARDDEVICE get /all /format:LIST
goto start

:LOGICALDISK
WMIC /Output:DiscosLogicos.txt  LOGICALDISK get /all /format:LIST
goto start

:PROCESS
WMIC /Output:Procesos.txt  PROCESS get /all /format:LIST
goto start

:RECOVEROS
WMIC /Output:ErroresSO.txt  RECOVEROS get /all /format:LIST
goto start

:STARTUP
WMIC /Output:ProgramasEnElInicio.txt  STARTUP get /all /format:LIST
goto start

:ERROR
cls
msg * Opcion no valida, prueba otra vez.
goto start


:SALIR
msg * Puede encontrar mas scripts utiles en http://norfipc.com 
EXIT