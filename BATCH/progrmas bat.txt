Hay que aclarar Batch que no es un Lenguaje de Programaci�n. Es un archivo de c�digo que contiene comandos del, Shell de Windows, llamado MS-DOS, que pueden ejecutar desde Inicio->Ejecutar->CMD.

Hay dos maneras de ejecutar comandos Batch.
Desde el Shell de Windows.
Escribi�ndolos en un archivo de texto con extensi�n .bat y luego ejecutarlo.


Hola Mundo en Batch


Comenzaremos creando un Simple y cl�sico "Hola Mundo", escribiendo en un archivo de texto el siguiente c�digo.
C�digo :
@echo off
echo Hola Mundo
pause
exit


Ahora guardemos el archivo como Nombre.bat y lo ejecutamos. Nos aparecer� una pantalla negra que dir� "Hola Mundo", luego una linea abajo dir� "Presione una tecla para continuar"
Explicamos:
echo:

Imprime un texto en pantalla, que es el texto que viene despu�s (el que le pasamos como par�metro), que en este caso es "Hola Mundo". Echo significa eco, por lo mismo "@echo off" elimina el eco, la repetici�n de la ruta en la que nos encontramos en cada l�nea c�digo.
pause:

Como bien se lee, pausa la ejecuci�n del c�digo. Adem�s muestra el texto "Presione una tecla para continuar"
exit:

Cierra la ventana de comandos.

Para dejar m�s en claro cual ser�a la diferencia de no ocupar la linea @echo off la imagen de a continuaci�n muestra como se ver�a la pantalla sin esta linea.




Ahora aprenderemos algunas funciones de Batch:

CD o CHDIR:

Si has trabajado en PHP, te sera f�cil entender este concepto. Sirve para cambiar de Directorio o Mostrar el nombre del actual. Para usarlo, CD o CHDIR seguido de la ubicaci�n del directorio al cual nos moveremos.
cd "C:\"
chdir "C:\"
Los ejemplos anteriores hacen exactamente lo mismo, cambian al Disco C

COPY:

Sirve para copiar uno o m�s archivos a otro destino.
Su sintaxis es la siguiente:
Copy "Direcci�n del archivo" "Nueva direcci�n"
Ejemplo:
C�digo :
Copy "C:\archivo.txt" "C:\archivo2.txt"

Aqu� copiar�amos un archivo del Disco C llamado "archivo.txt" como "archivo2.txt"
Para acortar un poco podr�amos usar:
C�digo :
cd "C:\"
Copy "archivo.txt" "archivo2.txt"

Donde primero cambiamos de directorio al Disco C con cd "C:\" y luego al copiar no tenemos que especificar que esta en el Disco C ya que ya nos encontramos en el.

DEL:

Sirve para eliminar uno o m�s archivos. Su sintaxis es:
Del "ARCHIVO"
Tambi�n se puede a�adir:
del /f /q "archivo"
Donde no pide confirmaci�n de eliminaci�n (/q) y fuerza al archivo a borrarse ( /f )

Move:

Sirve para mover un archivo. Su sintaxis es:
Move "Direcci�n Archivo" "Nueva Direcci�n"


Cls:

Sirve para limpiar el contenido de la pantalla:
Ejemplo:
C�digo :
@echo off
echo Hola, Cuando pulses la tecla se borrara el contenido y cambiara por otro.
pause
cls
echo �Ves?
pause
exit



Si quisi�ramos averiguar m�s sobre las funciones, o conocer algunas nuevas por nuestra propia cuenta podemos escribir en el Shell el nombre del comando seguido de /? y aparecer� toda la informaci�n necesaria.

Podemos poner en pr�ctica esto de la siguiente manera:
Abramos el Shell de windows:
Inicio->Ejecutar-CMD

Una vez abierto escribamos:
C�digo :
echo off

Para eliminar el eco. 
Luego para borrar el contenido de pantalla escribe:
C�digo :
cls

Ahora veamos la ayuda de la funci�n Copy:
C�digo :
copy /?

Cerremos la ventana:
C�digo :
exit


Ahora vamos a crear un Batch que copie un archivo, luego lo mueva a otro directorio, borre el original y vuelva a copiar el copiado.
En la carpeta donde crear�s el archivo .bat crea un archivo de texto llamado copiame.txt. En �l, escribe lo que quieras. Este ser� el archivo que copiaremos.
Ahora el Batch:

C�digo :
@echo off
echo Hola, Copiaremos un archivo
pause
copy "copiame.txt" "copiado.txt"
move "copiado.txt" "C:\copiado.txt"
del "copiame.txt"
cd "C:\"
copy "copiado.txt" "copiado2.txt"
exit

Gu�rdalo en la carpeta como NombreQueTuQuieras.bat, y ejec�talo.

Lo que ha hecho la cadena de instrucciones es m�s o menos lo siguiente:
Primero apagamos el eco, luego imprimimos en pantalla "Hola, Copiaremos un archivo"
Pausamos el int�rprete para que no siga de largo.
A continuaci�n, copiamos el archivo "Copiame.txt" y su copia se llamar� "copiado.txt". La movemos a la carpeta ra�z del disco C, y eliminamos el original.
Por ultimo cambiamos de directorio al disco C, copiamos la copia y cerramos el Batch.

Despu�s de que ejecutemos el Batch, si vamos al Disco C, veremos que hay dos nuevos archivos de texto: uno llamado copiado.txt y otro copiado2.txt.
Tambi�n si vamos a la carpeta del Batch, veremos no est� el archivo de texto original, "copiame.txt".

Creaci�n de nuevos archivos:


Con Batch podemos crear otros archivos. Para ello, tenemos que escribir algo como esto:
C�digo :
echo TEXTO DE PRIMERA LINEA > nombre.extensi�n
echo TEXTO SEGUNDA LINEA >> nombre.extensi�n
echo TEXTO SIGUIENTE LINEA >> nombre.extensi�n


Al escribir echo seguido de un texto y el signo > creamos un archivo con el nombre que siga.
Si ocupamos un echo seguido de un texto y el doble signo > (>>) se escribir� en la siguiente linea vac�a del archivo que prosiga.

Hagamos la prueba:

C�digo :
echo Creando un Archivo de texto > nuevo.txt
echo Esta es la Segunda linea >> nuevo.txt
echo Y esta es la Siguiente >> nuevo.txt
echo Esto esta genial >> nuevo.txt


Si ejecutamos eso, la pantalla negra se abrir� y cerrar�.
Pero en la carpeta en la que est� nuestro batch se abra creado un nuevo archivo "nuevo.txt" que si lo abrimos, dentro estar� escrito lo que especificamos.

Personalizando el SHELL


Seguro que no te gusta mucho el aspecto del Shell.
Aqu� veremos como personalizarlo un poco.

COLOR:

Con la funci�n color podemos cambiar los colores de fondo y las letras. Siendo su sintaxis:
COLOR AB
Donde A= El color de el Fondo y B= Color de las letras.
Para ver todos los colores ve el MS-Dos y escribe color/?

TITLE:

Para cambiar el t�tulo de la barra de tareas solo escribe:
Title Loquequierascomotitulo

Pause Personalizado:


�No te gusta que al poner pause salga "Presione una tecla para continuar..."?
Pues si escribes "pause>nul" no aparecer� ni un texto.
Entonces si anteponemos un mensaje, quedar�a as�.
C�digo :
echo Aprieta cualquier tecla para seguir
pause>nul


Adem�s puedes jugar con los caracteres. Aqu� un ejemplo, donde cambi� los colores y jugu� con los caracteres y personalic� el pause:




El c�digo que emple� para esto fue:
C�digo :
@echo off
color 30
echo ==================================
echo =                                =
echo =               Personalizado    =
echo =                                =
echo ==================================
echo.
echo.
echo Esto esta personalizado, para salir presiona una tecla.
pause>nul
exit


Cabe mencionar que echo. (Echo seguido de un punto)sirve para saltarse una l�nea. 

Matando Procesos:


�Alguna vez has presionado CTRL+ALT+DELETE?
Si lo haces se abre el Administrador de tarea. En la pesta�a procesos salen algunos procesos que est�n andando en tu pc, haci�ndoles clics y apretando "terminar proceso" lo puedes acabar.
Con batch eso es muy f�cil, se usa la funci�n:
Taskkill (No funciona en todos los pc, para ver si esta disponible escribe taskkill/?)
Su sintaxis es:
C�digo :
taskkill /f /im proceso.exe


/f Fuerza el t�rmino del proceso.

Un ejemplo seria escribir.
C�digo :
taskkill /f /im wmplayer.exe

Si lo ejecutas y tienes el Reproductor de Windows Media abierto, este se cerrar�.

ABRIENDO PROCESOS:


Ahora abramos ese proceso que cerramos. Para abrir procesos se usa la funci�n Start que funciona as�:
C�digo :
start proceso.exe

Tambi�n puedes abrir paginas web con tu explorador predeterminado.
Ejemplo1:


C�digo :
start wmplayer.exe

Se abrir� el Reproductor de M�sica
Ejemplo2:


C�digo :
start www.google.cl

Se abrir� tu Explorador de Internet en Google.

VARIABLES:



Ahora aprendamos a declarar variables. Para crear una se escribe:
C�digo :
set NOMBREVARIABLE = VALOR

Para llamarla debemos escribir su nombre entre los signos %%
Ejemplo:
C�digo :
@echo off
set Nombre= Javier Letelier Ruiz
echo %Nombre%
pause

La pantalla nos mostrara algo as�:




Para cambiar el valor solo debemos hacer esto:
C�digo :
set Nombre= Nuevo Valor

Si quieres que tu Variable sea din�mica, osea que el Usuario la elija antepones /p:

C�digo :
set /p Nombre=Escriba su Nombre:

Donde el Batch se pausara solo mostrando el Mensaje que va luego de "=" dejando que se escriba algo.
Tambi�n podemos multiplicarlas, sumarlas, etc... con /a:
C�digo :
set numero1=2
ser numero2=43
set /a suma= %numero1% + %numero2%
echo %suma%
pause
exit


Si ejecutamos este, nos devolver� la suma de 2 + 43, 45.

Creo que con esto ser� suficiente.
Ojal� hayan entendido y les haya interesado el batch, que principalmente se puede usar para facilitar algunas tareas de Windows, ya que como veremos en la segunda parte del tip se 
Uso de etiquetas


Como bien sabemos la lectura de los c�digos Batch se leen de una manera lineal, pero existe una forma para que el Batch se salte lineas, o vuelva a alguna anterior. Esto se logra con Etiquetas y la funci�n GOTO.
Para crear una etiqueta solo debemos escribir:
C�digo :
:nombreEtiqueta

Dos punto (:) seguido del nombre que le daremos a la etiqueta. De esta manera el GOTO funciona escribiendo 

GOTO seguido del nombre de la etiqueta, como vemos en el siguiente ejemplo.
C�digo :
@echo off
goto :mietiqueta
echo �Por que me saltan?
:MiEtiqueta
echo Hola, esta es la etiqueta y nos saltamos una parte del codigo.
pause>nul
exit


Dato Interesante:
En Batch las May�sculas y Min�sculas no se diferencian.

Condicionales:


El primer condicional que veremos el m�s usado. 
IF:
Se puede ocupar para comparar, cadenas, n�meros o para saber si existen archivos.
Ejemplo de sintaxis, if %cadena1% == %cadena2% OrdenAEjecutar
Que se podr�a leer de la siguiente manera. Si cadena1 es igual a Cadena2 ejecutemos la Orden OrdenAEjecutar. 
Tambi�n existe el NOT para ver si no son iguales.
if not %cadena1%== %cadena2% ejecutarAccion
Para saber si un archivo existe, se usa:
if exist "NombreDelArchivo" AccionAEjecutar
o tambi�n para ver si NO existe:
if not exist "NombreDelArchivo" AccionAEjecutar

No solo se puede ocupar el signo == para hacer comparaciones.
EQU, que es el equivalente a ==
NEQ, Sirve para cuando queremos ver Desigualdad.
LSS, Para verificar si el numero es Menor
LEQ, Para verificar si es menor o igual.
GTR, Si es mayor
GEQ, Si es Mayor e igual.

Un ejemplo de esto seria:
C�digo :
if 5 GTR 4 echo Hola

FOR:
Nos sirve para repetir un comando varias veces, en distintas variables.
Un ejemplo, es este:
for /L %%i IN (0,1,21) DO (echo %%i)
En donde /L es para especificar que nuestro for se basara en un conteo de n�meros. 
Aqu� se repetir� un Echo, que imprimir� n�meros del 0 al 21.
%%i sera el nombre de la variable que almacenara datos
in (0,1,21) aqu� especificamos que se repetir� del 0 al 1 y del 1 al 21. 
do (echo %%i) Esto es la funci�n que se ejecutara en cada repetici�n del comando, en este caso un echo que imprimir� la variable. 
Batch en algunos casos SI reconoce entre may�sculas y min�sculas, como en el caso de la variable %%i si esta se llamara %%I no seria reconocida como la misma.


Otro uso que puede ser aplicado en el FOR es el listado de carpetas o archivos, como veremos a continuaci�n:
Listado de archivos:
C�digo :
@echo off
FOR %%x in (a*) DO echo %%x
pause

Este c�digo, listar�a todos los archivos que comienzan con A.
�Por que? * (asterisco) es un signo comod�n, por lo cual al poner a*, estamos diciendo cualquier archivo que tenga una A delante y luego cualquier cosa.
Otro ejemplo del uso de comod�n, es listar archivos con extensi�n com�n como seria este:
C�digo :
@echo off
FOR %%x in (*.jpg) DO echo %%x
pause

Aqui nos estaria listando todos los archivos que tengan la extensi�n .jpg

Listado de Carpetas:
FOR /D %%x in (a*) DO echo %%x
Es casi lo mismo anterior, tan solo que al indicar /D solo se aplica a directorios. 
Si quisi�ramos que se listaran los directorios, juntos a los directorios que tienen estos dentro se ocupa /R
FOR /R /D %%x in (a*) DO echo %%x

y si quisi�ramos listar todos los documentos que est�n dentro de una carpeta contando los que est�n dentro de carpetas se ocupa solamente /R (Sin /D)
FOR /R %%x in (a*) DO echo %%x

Con esto creo que tenemos lo b�sico, y m�s importante de los condicionales. Talvez otro claber se anime a profundizar m�s en el tema. 

Creaci�n de Men�s:


Como ya vimos Condicionales y Etiquetas les ense�are una manera de hacer Men�s.
Comenzar�amos indicando las instrucciones y las opciones, de esta manera:
C�digo :
@echo off
:Menu
cls
echo Seleccione su opcion tecleando el numero respectivo.
echo.
echo 1. Primera Opcion
echo 2. Segunda Opcion
echo 3. Salir


Luego para seguir ocupar�amos esto:
C�digo :
set /p var=
if %var%==1 goto :Primero
if %var%==2 goto :Segundo
if %var%==3 goto exit
if %var% GTR 3 echo Error
goto :Menu

Con esto damos una variable en la que se guardara el numero insertado para luego ser verificado por los condicionales, si el numero es mayor que 3 nos avisara que no existe esa opci�n. Tambi�n al principio cree una etiqueta para que se pueda volver 

al men� y un "cls" para limpiar la pantalla.
Para terminar creamos las respectivas etiquetas.
C�digo :
:Primero
cls 
color a
Echo Esta es la Primera Opcion
Echo Precione una tecla para volver al menu
Pause>Nul
goto :Menu
:Segundo
cls 
color 1a
Echo Esta es la Segunda Opcion
Echo Precione una tecla para volver al menu
Pause>Nul
goto :Menu


Quedando para terminar nuestro codigo as�:
C�digo :
@echo off
:Menu
cls
echo Seleccione su opcion tecleando el numero respectivo.
echo.
echo 1. Primera Opcion
echo 2. Segunda Opcion
echo 3. Salir
set /p var=
if %var%==1 goto :Primero
if %var%==2 goto :Segundo
if %var%==3 goto exit
if %var% GTR 3 echo Error
goto :Menu
:Primero
cls 
color a
Echo Esta es la Primera Opcion
Echo Precione una tecla para volver al menu
Pause>Nul
goto :Menu
:Segundo
cls 
color 1a
Echo Esta es la Segunda Opcion
Echo Precione una tecla para volver al menu
Pause>Nul
goto :Menu

Bueno, con eso les basta para imaginarse como crear sus men�s personales.
Como vieron puede cada secci�n tener su color, incluso puedes cambiar la barra de titulo. 
Ve probando, no pierdes nada.

Apagado y Reiniciado de Pc:


Una cosa muy interesante que puedes hacer con Batch es Apagar y Reiniciar tu Pc, incluso programarlo para que apague a la hora que desees. 
Es notablemente f�cil.
se ocupa de la siguiente manera el apagado.
shutdown -s -t Tiempo -c "Comentario"
donde -s significa que lo apagaremos, -t debe ir seguido de los segundos que demorara apagar. En windos XP mostrara un contador, con el comentario que va luego de -c, pero en Windows Vista no abra ni un contador, solamente saldr� una alerta con el Comentario, que ni siquiera es obligatorio. 
Bueno, de seguro quieres hacer la prueba. La puedes hacer pero antes de eso te recomiendo que leas lo siguiente.
Puedes cancelar el apagado (Antes de que el contador termine) solamente escribiendo shutdown -a ya sea en un archivo de texto con extensi�n bat, como en el mismo Shell. 
Entonces, pues has la prueba
C�digo :
shutdown -s -t 999999 -c "Esto se esta apagando"

Tendr�s suficiente tiempo para ejecutar el siguiente c�digo. (11 d�as   )
C�digo :
shutdown -a

Con esto ya probamos los dos y podemos ser felices.
Ok, si en vez de ocupar -s usas -r el PC se reiniciara. 
Tambi�n puedes forzar el cierre de los programas, para que no de tiempo de guardar nada. Tan solo ocupando -f
Un ejemplo de un Pc, que se Apaga Forzosamente y que puede da�ar el pc y luego se reinicia es este:
C�digo :
shutdown -r -f -t 0 -c "Bye"

(No lo recomiendo probar en tu pc, Ya que apaga el pc en 0 segundos sin preguntar nada. )
Para programar un apagado lo veremos al final en los Datos Extras, ya que con la funci�n AT puedes programar el apagado. 


Leer archivos externos:


Para leer un archivo externo con Batch, como un archivo de texto por ejemplo. Se usa el siguiente c�digo:
C�digo :
type "NombreDelArchivo.txt"

�Simple no? 

Insertar al Registro:


Insertar al registro tu Batch puede ser muy �til, la verdad yo no entiendo mucho del registro pero le� un poco y aqu� tienen un ejemplo que se encuentra a ser sincero en muchas web. Donde aremos que nuestro batch se ejecute con el Sistema. 

C�digo :
REG ADD HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run /v MIBATCH.bat /t REG_SZ /d 

C:\MIBATCH.bat


Con la funci�n REG ADD Agregamos algo al registro. Con otros comando podemos borrar.
Lo que sigue es la "Direcci�n" de donde insertaremos el registro que en este caso sera para que nuestro batch se ejecute al iniciar Sesi�n. 
En medio de toda la direcci�n se puede leer MIBATCH.bat yo puse el nombre del Batch pero tambi�n se puede poner cualquier otra cosa. M�s adelante donde dice C:\MIBATCH.bat, es la direcci�n completa de donde esta nuestro Batch.
Con solo esta linea estamos listos. Haz la prueba!

Otras cosas �tiles:


Para terminar hablare de peque�as funciones que pueden ser de utilidad.

Msg 
Esta funci�n tiene otras utilidades, pero les ense�are esta que no funciona en todos los PC.
Sirve para abrir una alerta con un mensaje su utilizaci�n es la siguiente:
C�digo :
msg * EsteEsMiMensaje


AT
Esta es una de las que m�s me gusta, es para programar alguna acci�n a cierta hora. 
Ejemplo:
C�digo :
At 20:00 msg * Son Las 8 de la noche

Con esto a las 20:00 nos aparecer� un mensaje que nos dir� que son las 8 de la Noche. 
Tambi�n puedes programar un apagado o cualquier Cosa.
Si la hora que das ya paso, se tomara como para el d�a siguiente.

Creo que he terminado, me cost� un poco. Ojala no me haya faltado nada. Disfr�tenlo y seria bueno que los Clabers aporten con m�s informaci�n. Yo creo que seria �til para todos, incluy�ndome. 
Suerte.

 curso batch desde cero	
este curso lo hago para aquellas personas que no saben programar en batch pero les gusta aprender. 
sin mas preambulo empezemos. 
para programar en batch solo nececitamos el bloc de notas de windows. 
empezaremos usando el comando " shutdown " 
el comando shutdown nos sirve para apagar la pc, este comando lo podemos combinar con letras para obtener otros resultados. 
paremos la teoria y hagamos un ejemplo : 

abrimos el bloc de notas (inicio-ejecutar-notepad) 
y ahi escribimos: 

shutdown -s -t 30 -c "APAGANDO PC" (lo escribimos tal como esta). 

ahora vamos a archivo - guardar como... 
ahi le ponemos cualquier nombre que termine con extencion .bat 
ejemplo : apagando.bat 
y abajo donde dice 
TIPO: le cambiamos a TODOS LOS ARCHIVOS. 
y le damos guardar. 
ahora abrimos el archivo creado y hemos creado nuestro primer virus. 
si este archivo lo guardamos en la carpeta de inicio de windows tendremos un gran problema ya que la pc no estara mas de 30 segundos prendida.. 

bueno ahora bamos a programar un apagado automatico. 
para hacer esto utilizaremos el comando shutdown junto con las letras at 

empezemos: 
ahora lo bamos a hacer desde inicio - ejecutar - cmd 
y en la ventana que nos abre bamos a escribir 

at (aqui la hora a la que quieren que la pc se apague) shutdown -s -f -t15 -c "Apagado automatico" 
presionamos enter y tenemos programado el pc a la hora que tu le pusiste 
y lo guardamos con extencion .bat 
en este ejemplo 
-s = apagar la pc 
-f = fuerza a que todas las aplicaciones se cierren 
-t = ponemos 15 segundos de plaso mientras cierra las aplicaciones 
-c = poner comentario 

con el comando at tambien podemos programar un batch para que se ejecute a cierta hora. 
creemos una carpeta en C:\Archivos de Programa 
le ponemos cualquier nombre yo le voy a poner proyecto batch 
y en esta carpeta guardamos un .bat que haga cualquier cosa. 
los comandos de el batch que guarde en esta carpeta es: 

@echo off 
echo ya son las 3:00 
echo no estes tanto tiempo en frente de esta pc 
echo estas cocinado tu mente 
pause 
exit 

y ahora entramos a 
inicio-ejecutar-cmd y escribimos 
AT (cualquier hora) C:\Archivos de Programa\proyecto batch\nombre_del batch_creado 

y nos saldra un mensaje que dice 
se ha agregado un nuevo trabajo con identificador=1 

con el comando at haremos que el batch que tenemos guardado se ejecute a determinada hora. 
ahorita seguimos con mas de batch

